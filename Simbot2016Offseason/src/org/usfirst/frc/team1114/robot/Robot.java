
package org.usfirst.frc.team1114.robot;

import org.usfirst.frc.team1114.robot.auton.AutonControl;
import org.usfirst.frc.team1114.robot.io.DriverInput;
import org.usfirst.frc.team1114.robot.io.Logger;
import org.usfirst.frc.team1114.robot.io.RobotOutput;
import org.usfirst.frc.team1114.robot.io.SensorInput;
import org.usfirst.frc.team1114.robot.teleop.TeleopControl;
import org.usfirst.frc.team1114.robot.util.RobotConstants;

import edu.wpi.first.wpilibj.IterativeRobot;

public class Robot extends IterativeRobot {
    
	private RobotOutput robotOut;
	private DriverInput driverInput;
	private SensorInput sensorInput;
	private TeleopControl teleopControl;
    private Logger logger;
    
    @Override
	public void robotInit() {
    	RobotConstants.pushValues();
    	this.robotOut = RobotOutput.getInstance();
    	this.driverInput = DriverInput.getInstance();
    	this.sensorInput = SensorInput.getInstance();
    	this.teleopControl = TeleopControl.getInstance();	
    	this.logger = Logger.getInstance();
    }
    
    @Override
	public void disabledInit(){
    	this.robotOut.stopAll();
    	this.teleopControl.disable();
    	this.logger.close();
    }
    
    @Override
	public void disabledPeriodic(){
    	this.sensorInput.update();
    	AutonControl.getInstance().updateModes();
    }
    
	@Override
	public void autonomousInit() {
		AutonControl.getInstance().initialize();
		SensorInput.getInstance().reset();
		this.logger.openFile();
    }
    
    @Override
	public void autonomousPeriodic() {
    	this.sensorInput.update();
    	AutonControl.getInstance().runCycle();
    	this.logger.logAll();
    }
    
    @Override
	public void teleopInit(){
    	//this.logger.openFile();
    }

    @Override
	public void teleopPeriodic() {
    	this.sensorInput.update();
    	this.teleopControl.runCycle();
    	//this.logger.logAll();
    }
    
    
    
    
   
    
}
