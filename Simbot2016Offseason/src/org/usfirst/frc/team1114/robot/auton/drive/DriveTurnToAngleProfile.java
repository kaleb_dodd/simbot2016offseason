package org.usfirst.frc.team1114.robot.auton.drive;

import org.usfirst.frc.team1114.robot.auton.AutonCommand;
import org.usfirst.frc.team1114.robot.auton.RobotComponent;
import org.usfirst.frc.team1114.robot.io.RobotOutput;
import org.usfirst.frc.team1114.robot.io.SensorInput;
import org.usfirst.frc.team1114.robot.util.RobotConstants;
import org.usfirst.frc.team1114.robot.util.SimLib;
import org.usfirst.frc.team1114.robot.util.SimMotionProfile;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class DriveTurnToAngleProfile extends AutonCommand {

	private RobotOutput robotOut;
	private SensorInput sensorIn;
	
	private SimMotionProfile gyroControl;
	
	private boolean firstCycle = true;
	private double target;
	private double maxOutput;
	
	public DriveTurnToAngleProfile(double target, double eps, long timeOut){
		this(target,-1,eps,timeOut);
	}
	
	public DriveTurnToAngleProfile(double target, double maxVel, double eps, long timeOut){
		this(target,maxVel,eps,-1,timeOut);
	}
	
	
	public DriveTurnToAngleProfile(double target,double maxVel,double eps, double epsVel, long timeOut){
		this(target,maxVel,-1,eps,epsVel,timeOut);
	}
	
	
	public DriveTurnToAngleProfile(double target, double maxVel, double maxAccel, double eps, double epsVel, long timeOut){
		this(target,maxVel,maxAccel,maxAccel,1.0,eps,epsVel,timeOut);
	}
	
	public DriveTurnToAngleProfile(double target,double maxVel, double maxAccel, double maxDecel,double maxOutput,double eps,double epsVel ,long timeOut) {
		super(RobotComponent.DRIVE, timeOut);
		
		this.target = target; 
		this.maxOutput = maxOutput;
		this.robotOut = RobotOutput.getInstance();
		this.sensorIn = SensorInput.getInstance();
		
		
		
		
		double gP = SmartDashboard.getNumber("Profile Gyro P: ", RobotConstants.gyroP);
		double gI = SmartDashboard.getNumber("Profile Gyro I: ", RobotConstants.gyroI);
		double gD = SmartDashboard.getNumber("Profile Gyro D: ", RobotConstants.gyroD);
		double gKv = SmartDashboard.getNumber("Profile Gyro VelFF: ", RobotConstants.gyroKv);
		double gKF = SmartDashboard.getNumber("Profile Gyro AcclFF: ", RobotConstants.gyroKa);
		double gEps;
		
		if(eps != -1){
			gEps = eps;
		}else{	
			gEps = SmartDashboard.getNumber("Profile Gyro Eps: ", RobotConstants.gyroEps);
		}
		
		if(maxVel != -1){
			RobotConstants.GYRO_MAX_VEL = (maxVel);
		}
		
		if(maxAccel != -1){
			RobotConstants.GYRO_MAX_ACCEL= (maxAccel);
		}
		
		if(maxDecel != -1){
			RobotConstants.GYRO_MAX_DECEL= (maxDecel);
		}
				
		if(epsVel == -1){
			epsVel = 0.01;
		}
		
		this.gyroControl = new SimMotionProfile(gP,gI,gD,gKv,gKF,gEps, 0.1);
		this.gyroControl.configureProfile(this.sensorIn.getDeltaTime(), RobotConstants.GYRO_MAX_VEL, RobotConstants.GYRO_MAX_ACCEL, RobotConstants.GYRO_MAX_DECEL);
		this.gyroControl.setDebug(false);
		
		
	}

	@Override
	public boolean calculate() {
		if(this.firstCycle){
			this.firstCycle = false;
			double angle = this.sensorIn.getAngle();
            double offset = angle % 360;
            
            double p = this.sensorIn.getGyroPositionState();
            double v = this.sensorIn.getGyroVelocityState();
            double a = this.sensorIn.getGyroAccelerationState();
            
            
            if (this.target - offset < -180){
                this.gyroControl.setDesiredValue(p,v,a,angle + 360 + this.target - offset);         
            } else if (this.target - offset < 180) {
                this.gyroControl.setDesiredValue(p,v,a,angle + this.target - offset);
            } else {
                this.gyroControl.setDesiredValue(p,v,a,angle - 360 + this.target - offset);
            }
		}
		
		double yVal = 0;
		double xVal = -this.gyroControl.calculate(this.sensorIn.getAngle(), this.sensorIn.getGyroVelocity());
		
		
		double leftDrive = SimLib.calcLeftTankDrive(xVal, yVal);
		double rightDrive = SimLib.calcRightTankDrive(xVal, yVal);
		
		if(leftDrive > this.maxOutput){ // going too fast
			leftDrive = this.maxOutput;
		}else if(leftDrive < -this.maxOutput){
			leftDrive = -this.maxOutput;
		}
		
		if(rightDrive > this.maxOutput){ // going too fast
			rightDrive = this.maxOutput;
		}else if(rightDrive < -this.maxOutput){
			rightDrive = -this.maxOutput;
		}
		
		
		if(this.gyroControl.isDone()){
			this.robotOut.setDriveLeft(0.0);
			this.robotOut.setDriveRight(0.0);
			System.out.println("Control Finished");
			return true;
		}else{
			this.robotOut.setDriveLeft(leftDrive);
			this.robotOut.setDriveRight(rightDrive);
			System.out.println("Control NOT Finished");
			return false;
		}
		
		
		
	}

	@Override
	public void override() {
		// TODO Auto-generated method stub
		this.robotOut.setDriveLeft(0.0);
		this.robotOut.setDriveRight(0.0);
	}

}
