package org.usfirst.frc.team1114.robot.auton.mode.step2Modes;

import org.usfirst.frc.team1114.robot.auton.drive.DriveTurnToAngleProfile;
import org.usfirst.frc.team1114.robot.auton.drive.DriveWait;
import org.usfirst.frc.team1114.robot.auton.mode.AutonBuilder;
import org.usfirst.frc.team1114.robot.auton.mode.AutonMode;

public class Turn90Test implements AutonMode {

	@Override
	public void addToMode(AutonBuilder ab) {
		ab.addCommand(new DriveTurnToAngleProfile(180, 10, 1.0, 1.0, 1.0, 0.5, 0.1, -1));
		ab.addCommand(new DriveWait());
		
	}

}
