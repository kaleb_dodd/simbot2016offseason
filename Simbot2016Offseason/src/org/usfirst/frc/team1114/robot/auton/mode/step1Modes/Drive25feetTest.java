package org.usfirst.frc.team1114.robot.auton.mode.step1Modes;

import org.usfirst.frc.team1114.robot.auton.drive.DriveAtOutputForDistance;
import org.usfirst.frc.team1114.robot.auton.drive.DriveWait;
import org.usfirst.frc.team1114.robot.auton.mode.AutonBuilder;
import org.usfirst.frc.team1114.robot.auton.mode.AutonMode;

public class Drive25feetTest implements AutonMode {

	@Override
	public void addToMode(AutonBuilder ab) {
		ab.addCommand(new DriveAtOutputForDistance(1.0, 25));
		ab.addCommand(new DriveWait());
	}

}
