package org.usfirst.frc.team1114.robot.auton.mode.step1Modes;

import org.usfirst.frc.team1114.robot.auton.drive.DriveStraightProfile;
import org.usfirst.frc.team1114.robot.auton.drive.DriveWait;
import org.usfirst.frc.team1114.robot.auton.mode.AutonBuilder;
import org.usfirst.frc.team1114.robot.auton.mode.AutonMode;

public class DriveStraightTest implements AutonMode {

	@Override
	public void addToMode(AutonBuilder ab) {
			ab.addCommand(new DriveStraightProfile(8, 5, 5, 5, 1, 0.1, 0.01,-1));
			ab.addCommand(new DriveWait());
		
		
	}

}
