package org.usfirst.frc.team1114.robot.auton.drive;

import org.usfirst.frc.team1114.robot.auton.AutonCommand;
import org.usfirst.frc.team1114.robot.auton.RobotComponent;
import org.usfirst.frc.team1114.robot.io.RobotOutput;

public class DriveSetOutput extends AutonCommand {

	private RobotOutput robotOut;
	private double output;
	
	public DriveSetOutput(double output) {
		super(RobotComponent.DRIVE);
		this.output = output;
		this.robotOut = RobotOutput.getInstance();
		
	}

	@Override
	public boolean calculate() {
		this.robotOut.setDriveLeft(this.output);
		this.robotOut.setDriveRight(this.output);
		return true;
	}

	@Override
	public void override() {
		// TODO Auto-generated method stub
		
	}

}
