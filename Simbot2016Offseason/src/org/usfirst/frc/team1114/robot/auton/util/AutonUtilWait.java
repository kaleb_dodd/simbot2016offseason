package org.usfirst.frc.team1114.robot.auton.util;

import org.usfirst.frc.team1114.robot.auton.AutonCommand;
import org.usfirst.frc.team1114.robot.auton.RobotComponent;

/**
 *
 * @author Michael
 */
public class AutonUtilWait extends AutonCommand {

    public AutonUtilWait() {
        super(RobotComponent.UTIL);
    }

    @Override
	public boolean calculate() {
        return true;
    }

	@Override
	public void override() {
		// nothing to do
		
	}
    
    
    
}
