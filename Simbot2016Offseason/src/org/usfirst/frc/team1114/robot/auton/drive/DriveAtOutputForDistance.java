package org.usfirst.frc.team1114.robot.auton.drive;

import org.usfirst.frc.team1114.robot.auton.AutonCommand;
import org.usfirst.frc.team1114.robot.auton.RobotComponent;
import org.usfirst.frc.team1114.robot.io.RobotOutput;
import org.usfirst.frc.team1114.robot.io.SensorInput;

public class DriveAtOutputForDistance extends AutonCommand {

	private SensorInput sensorIn;
	private RobotOutput robotOut;
	private double output;
	private double distance;
	private double startPosition;
	private boolean firstCycle = true;
	
	public DriveAtOutputForDistance(double output, double distance) {
		super(RobotComponent.DRIVE,-1);
		this.sensorIn = SensorInput.getInstance();
		this.robotOut = RobotOutput.getInstance();
		this.output = output;
		this.distance = distance;
	}

	@Override
	public boolean calculate() {
		if(this.firstCycle){
			this.startPosition = this.sensorIn.getDrivePositionState();
			this.firstCycle = false;
		}
		
		double currentPosition = this.sensorIn.getDrivePositionState();
		if(currentPosition > (this.startPosition + this.distance)){ // we have passed the point so stop now
			this.robotOut.setDriveLeft(0);
			this.robotOut.setDriveRight(0);
			return true;
		}else{
			this.robotOut.setDriveLeft(this.output);
			this.robotOut.setDriveRight(this.output);
			return false;
		}
		
		
	
	}

	@Override
	public void override() {
		this.robotOut.setDriveLeft(0);
		this.robotOut.setDriveRight(0);
	}

}
