package org.usfirst.frc.team1114.robot.auton.util;

import org.usfirst.frc.team1114.robot.auton.AutonCommand;
import org.usfirst.frc.team1114.robot.auton.AutonControl;
import org.usfirst.frc.team1114.robot.auton.RobotComponent;

/**
 *
 * @author Michael
 */
public class AutonStop extends AutonCommand {

    public AutonStop() {
        super(RobotComponent.UTIL);
    }
    
    @Override
	public boolean calculate() {
        AutonControl.getInstance().stop();
        return true;
    }

	@Override
	public void override() {
		// nothing to do
		
	}
    
}
