package org.usfirst.frc.team1114.robot.auton.drive;

import org.usfirst.frc.team1114.robot.auton.AutonCommand;
import org.usfirst.frc.team1114.robot.auton.RobotComponent;
import org.usfirst.frc.team1114.robot.io.RobotOutput;
import org.usfirst.frc.team1114.robot.io.SensorInput;
import org.usfirst.frc.team1114.robot.util.SimLib;
import org.usfirst.frc.team1114.robot.util.SimPID;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class DriveStraightPID extends AutonCommand {

	private RobotOutput robotOut;
	private SensorInput sensorIn;
	
	private SimPID encControl;
	private SimPID gyroControl;
	
	private double gyroP;
	private double gyroI;
	private double gyroD;
	private double gyroEps;
	
	private double encP;
	private double encI;
	private double encD;
	private double encEps; 
	
	private boolean firstCycle;
	private double target;
	
	public DriveStraightPID(double target, long timeOut){
		this(target, -1,1.0,timeOut);
	}
	
	public DriveStraightPID(double target,double eps){
		this(target, eps, 1.0);
	}
	
	public DriveStraightPID(double target, double eps,double maxOutput){
		this(target, eps, maxOutput, -1);
	}
	
	public DriveStraightPID(double target, double eps,long timeOut){
		this(target, eps, 1.0,timeOut);
	}
	
	public DriveStraightPID(double target,double eps,double maxOutput,long timeOut) {
		super(RobotComponent.DRIVE,timeOut);
		
		this.robotOut = RobotOutput.getInstance();
		this.sensorIn = SensorInput.getInstance();
		
		this.target = target;
		this.firstCycle = false;
		
		this.encP = SmartDashboard.getNumber("Drive Enc P: ");
		this.encI = SmartDashboard.getNumber("Drive Enc I: ");
		this.encD = SmartDashboard.getNumber("Drive Enc D: ");
		if(eps != -1){
			this.encEps = eps;
		}else{	
			this.encEps = SmartDashboard.getNumber("Drive Enc Eps: ");
		}
		
		this.gyroP = SmartDashboard.getNumber("Drive Gyro P: ");
		this.gyroI = SmartDashboard.getNumber("Drive Gyro I: ");
		this.gyroD = SmartDashboard.getNumber("Drive Gyro D: ");
		this.gyroEps = SmartDashboard.getNumber("Drive Gyro Eps: ");
		
		this.encControl = new SimPID(this.encP,this.encI,this.encD,this.encEps);
		this.encControl.setFinishedRange(this.encEps);
		this.encControl.setMaxOutput(maxOutput);
		
		this.gyroControl = new SimPID(this.gyroP,this.gyroI,this.gyroD,this.gyroEps);
		
		
	}

	@Override
	public boolean calculate() {
		if(this.firstCycle){
			this.encControl.setDesiredValue(this.sensorIn.getDriveInches() + this.target);
			this.gyroControl.setDesiredValue(this.sensorIn.getAngle());
			this.firstCycle = false;
		}
		
		double yVal = this.encControl.calcPID(this.sensorIn.getDriveInches());
		double xVal = -this.gyroControl.calcPID(this.sensorIn.getAngle());
		
		double leftDrive = SimLib.calcLeftTankDrive(xVal, yVal);
		double rightDrive = SimLib.calcRightTankDrive(xVal, yVal);
		
		if(this.encControl.isDone()){
			this.robotOut.setDriveLeft(0.0);
			this.robotOut.setDriveRight(0.0);
			return true;
		}else{
			this.robotOut.setDriveLeft(leftDrive);
			this.robotOut.setDriveRight(rightDrive);
			return false;
		}
	}

	@Override
	public void override() {
		this.robotOut.setDriveLeft(0.0);
		this.robotOut.setDriveRight(0.0);
		
	}

}
