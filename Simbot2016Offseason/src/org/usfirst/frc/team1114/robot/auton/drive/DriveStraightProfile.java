package org.usfirst.frc.team1114.robot.auton.drive;

import org.usfirst.frc.team1114.robot.auton.AutonCommand;
import org.usfirst.frc.team1114.robot.auton.RobotComponent;
import org.usfirst.frc.team1114.robot.io.RobotOutput;
import org.usfirst.frc.team1114.robot.io.SensorInput;
import org.usfirst.frc.team1114.robot.util.RobotConstants;
import org.usfirst.frc.team1114.robot.util.SimLib;
import org.usfirst.frc.team1114.robot.util.SimMotionProfile;
import org.usfirst.frc.team1114.robot.util.SimMotionProfileContinuous;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class DriveStraightProfile extends AutonCommand {

	private RobotOutput robotOut;
	private SensorInput sensorIn;
	
	private SimMotionProfile encControl;
	private SimMotionProfileContinuous gyroControl;
	
	private boolean firstCycle = true;
	private double target;
	private double maxOutput;
	
	
	public DriveStraightProfile(double target, double eps, long timeOut){
		this(target,-1,eps,timeOut);
	}
	
	public DriveStraightProfile(double target, double maxVel, double eps,long timeOut){
		this(target,maxVel,eps,-1,timeOut);
	}
	
	public DriveStraightProfile(double target,double maxVel, double eps, double epsVel, long timeOut){
		this(target,maxVel,-1,eps,epsVel,timeOut);
	}
	
	
	public DriveStraightProfile(double target, double maxVel, double maxAccel, double eps, double epsVel, long timeOut){
		this(target,maxVel,maxAccel,maxAccel,1.0,eps,epsVel,timeOut);
	}
	
	
	
	public DriveStraightProfile(double target,double maxVel, double maxAccel, double maxDecel,double maxOutput,double eps,double epsVel ,long timeOut) {
		super(RobotComponent.DRIVE, timeOut);
		
		this.target = target; 
		this.maxOutput = maxOutput;
		this.robotOut = RobotOutput.getInstance();
		this.sensorIn = SensorInput.getInstance();
		
		
		
		double eP = SmartDashboard.getNumber("Profile Enc P: ", RobotConstants.encP);
		double eI = SmartDashboard.getNumber("Profile Enc I: ", RobotConstants.encI);
		double eD = SmartDashboard.getNumber("Profile Enc D: ", RobotConstants.encD);
		double eKv = SmartDashboard.getNumber("Profile Enc VelFF: ", RobotConstants.encKv);
		double eKF = SmartDashboard.getNumber("Profile Enc AcclFF: ", RobotConstants.encKa);
		double eEps;
				
		if(eps != -1){
			eEps = eps;
		}else{	
			eEps = SmartDashboard.getNumber("Profile Enc Eps: ", RobotConstants.encEps);
		}
		
		double gP = SmartDashboard.getNumber("Profile Gyro P: ", RobotConstants.gyroP);
		double gI = SmartDashboard.getNumber("Profile Gyro I: ", RobotConstants.gyroI);
		double gD = SmartDashboard.getNumber("Profile Gyro D: ", RobotConstants.gyroD);
		double gKv = SmartDashboard.getNumber("Profile Gyro VelFF: ", RobotConstants.gyroKv);
		double gKF = SmartDashboard.getNumber("Profile Gyro AcclFF: ", RobotConstants.gyroKa);
		double gEps = SmartDashboard.getNumber("Profile Gyro Eps: ", RobotConstants.gyroEps);
		
		if(maxVel != -1){
			RobotConstants.DRIVE_MAX_VEL= (maxVel);
		}
		
		if(maxAccel != -1){
			RobotConstants.DRIVE_MAX_ACCEL= (maxAccel);
		}
		
		if(maxDecel != -1){
			RobotConstants.DRIVE_MAX_DECEL = (maxDecel);
		}
				
		if(epsVel == -1){
			epsVel = 0.01;
		}
		
		this.encControl = new SimMotionProfile(eP,eI,eD,eKv,eKF,eEps,epsVel);
		this.gyroControl = new SimMotionProfileContinuous(gP,gI,gD,gKv,gKF,gEps, 0.1);
		
		this.encControl.configureProfile(this.sensorIn.getDeltaTime(), RobotConstants.DRIVE_MAX_VEL, RobotConstants.DRIVE_MAX_ACCEL, RobotConstants.DRIVE_MAX_DECEL);
		this.gyroControl.configureProfile(this.sensorIn.getDeltaTime(), RobotConstants.GYRO_MAX_VEL, RobotConstants.GYRO_MAX_ACCEL, RobotConstants.GYRO_MAX_DECEL);
		
		this.encControl.setDebug(true);
		this.gyroControl.setDebug(false);
		
		
	}

	@Override
	public boolean calculate() {
		if(this.firstCycle){
			this.encControl.setDesiredValue(this.sensorIn.getDrivePositionState(),this.sensorIn.getDriveVelocityState(),
					this.sensorIn.getDriveAccelerationState(), this.sensorIn.getDriveFeet() + this.target); // sets the targets and gives the robots starting state
			//this.gyroControl.setDesiredValue(sensorIn.getGyroState(), this.sensorIn.getAngle());
			this.gyroControl.setDesiredValue(this.sensorIn.getGyroPositionState(), this.sensorIn.getGyroVelocityState(),
					this.sensorIn.getGyroAccelerationState(), this.sensorIn.getAngle());
			this.firstCycle = false;
			System.out.println("First Cycle");
		}
		
		double yVal = this.encControl.calculate(this.sensorIn.getDriveFeet(), this.sensorIn.getDriveSpeedFPS());
		//double xVal = this.gyroControl.calculate(this.sensorIn.getAngle(), this.sensorIn.getGyroVelocity());
		double xVal = 0; // just for testing / tuning 
		
		double leftDrive = SimLib.calcLeftTankDrive(xVal, yVal);
		double rightDrive = SimLib.calcRightTankDrive(xVal, yVal);
		
		if(leftDrive > this.maxOutput){ // going too fast
			leftDrive = this.maxOutput;
		}else if(leftDrive < -this.maxOutput){
			leftDrive = -this.maxOutput;
		}
		
		if(rightDrive > this.maxOutput){ // going too fast
			rightDrive = this.maxOutput;
		}else if(rightDrive < -this.maxOutput){
			rightDrive = -this.maxOutput;
		}
		
		this.gyroControl.isDone(this.sensorIn.getGyroPositionState(), this.sensorIn.getGyroVelocityState(),
				this.sensorIn.getGyroAccelerationState()); // check if we are done and then recalculate a new profile
		
		if(this.encControl.isDone()){
			this.robotOut.setDriveLeft(0.0);
			this.robotOut.setDriveRight(0.0);
			System.out.println("CONTROL FINISHED");
			return true;
		}else{
			this.robotOut.setDriveLeft(leftDrive);
			this.robotOut.setDriveRight(rightDrive);
			System.out.println("CONTROL NOT FINISHED");
			return false;
		}
		
		
		
	}

	@Override
	public void override() {
		// TODO Auto-generated method stub
		this.robotOut.setDriveLeft(0.0);
		this.robotOut.setDriveRight(0.0);
	}

}
