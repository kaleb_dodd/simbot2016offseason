package org.usfirst.frc.team1114.robot.teleop;

import org.usfirst.frc.team1114.robot.io.DriverInput;
import org.usfirst.frc.team1114.robot.io.RobotOutput;
import org.usfirst.frc.team1114.robot.io.SensorInput;
import org.usfirst.frc.team1114.robot.util.SimBezierCurve;
import org.usfirst.frc.team1114.robot.util.SimLib;
import org.usfirst.frc.team1114.robot.util.SimPoint;

public class TeleopDrive implements TeleopComponent {

	private static TeleopDrive instance;

	private RobotOutput robotOut;
	private DriverInput driverIn;
	private SensorInput sensorIn;
	
	private SimBezierCurve SimCurveXH;
	private SimBezierCurve SimCurveYH;

	private boolean firstCycle = true;
	
	public static TeleopDrive getInstance() {
		if (instance == null) {
			instance = new TeleopDrive();
		}
		return instance;
	}

	private TeleopDrive() {

		this.driverIn = DriverInput.getInstance();
		this.robotOut = RobotOutput.getInstance();
		this.sensorIn = SensorInput.getInstance();

		
		SimPoint zero = new SimPoint(0, 0);
		//High gear
		SimPoint xP1H = new SimPoint(0.0, 0.30);
		SimPoint xP2H = new SimPoint(0.45, -0.1);

		SimPoint yP1H = new SimPoint(0.0, 0.54);
		SimPoint yP2H = new SimPoint(0.45, -0.07);
		
		SimPoint one = new SimPoint(1, 1);
		
		this.SimCurveXH = new SimBezierCurve(zero, xP1H, xP2H, one);
		this.SimCurveYH = new SimBezierCurve(zero, yP1H, yP2H, one);
		
	
		
	}

	@Override
	public void calculate() {

		// driver inputs for drive
		if(this.driverIn.getDriverHighGearButton()) {
			this.robotOut.setHighGear(true);
		} else if (this.driverIn.getDriverLowGearButton()) {
			this.robotOut.setHighGear(false);
		}
		
		double x;
		double y;
		if(Math.abs(this.driverIn.getDriverX()) < 0.05){
			x = 0;
		}else{
			x = this.SimCurveXH.getPoint(this.driverIn.getDriverX()).getY();
		}
		
		if(Math.abs(this.driverIn.getDriverY())  < 0.1){
			y = 0;
		}else{
			y = this.SimCurveYH.getPoint(this.driverIn.getDriverY()).getY();
		}
		
		// arcade calculations
		double leftOut = SimLib.calcLeftTankDrive(x, y);
		double rightOut = SimLib.calcRightTankDrive(x, y);

		// outputs to motors
		this.robotOut.setDriveLeft(leftOut);
		this.robotOut.setDriveRight(rightOut);
		
	}

	@Override
	public void disable() {
		this.robotOut.setDriveLeft(0.0);
		this.robotOut.setDriveRight(0.0);
	}

	
}
