package org.usfirst.frc.team1114.robot.teleop;

import org.usfirst.frc.team1114.robot.io.DriverInput;
import org.usfirst.frc.team1114.robot.io.RobotOutput;
import org.usfirst.frc.team1114.robot.io.SensorInput;
import org.usfirst.frc.team1114.robot.util.SimBezierCurve;
import org.usfirst.frc.team1114.robot.util.SimPoint;

public class TeleopIntake implements TeleopComponent {
	private static TeleopIntake instance;
	private RobotOutput robotOut;
	private DriverInput driverIn;
	private SensorInput sensorIn;
	
	private SimBezierCurve intakeCurve;
	private double intakeOut = 0;
	
	public static TeleopIntake getInstance(){
		if(instance == null){
			instance = new TeleopIntake();
		}
		return instance;
	}
	
	private TeleopIntake() {
		this.driverIn = DriverInput.getInstance();
		this.robotOut = RobotOutput.getInstance();
		this.sensorIn = SensorInput.getInstance();
		SimPoint zero = new SimPoint(0,0);
		
		SimPoint yP1 = new SimPoint(0.0,0.37);
		SimPoint yP2 = new SimPoint(0.87,0.41);
		
		SimPoint one = new SimPoint(1,1);
		this.intakeCurve = new SimBezierCurve(zero,yP1,yP2,one);
	}

	@Override
	public void calculate() {
		double y = this.intakeCurve.getPoint(this.driverIn.getDriverRightY()).getY();
		this.intakeOut = y*0.75;
		
		this.robotOut.setIntakeArm(this.intakeOut);

	}

	@Override
	public void disable() {
		this.robotOut.setIntakeArm(0);
	}

}
