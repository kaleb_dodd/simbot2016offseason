package org.usfirst.frc.team1114.robot.teleop;

/**
 *
 * @author jason
 */
public interface TeleopComponent {
    public void calculate();
    public void disable();
}
