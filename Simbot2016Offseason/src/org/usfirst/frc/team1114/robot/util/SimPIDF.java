package org.usfirst.frc.team1114.robot.util;

import org.usfirst.frc.team1114.robot.io.SensorInput;

public class SimPIDF extends SimPID{
	
	private SensorInput sensorIn;
	private double feedForward;
	
	public SimPIDF(double p, double i, double d,double f,double eps){
		super(p, i, d,eps);
		this.sensorIn = SensorInput.getInstance();
		this.feedForward = f;
	}
	
	@Override
	public double calcPID(double current){
		return super.calcPID(current) + (super.getDesiredVal()*this.feedForward); 
	}
	
	public void setConstants(double p, double i, double d, double f){
		super.setConstants(p, i, d);
		this.feedForward = f;
	}
	
	public void setFeedForward(double f){
		this.feedForward = f;
	}

}
