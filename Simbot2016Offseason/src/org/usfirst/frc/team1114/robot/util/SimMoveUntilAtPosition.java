package org.usfirst.frc.team1114.robot.util;

public class SimMoveUntilAtPosition extends SimMotionProfile{

	public SimMoveUntilAtPosition(double p, double i, double d, double vFF, double aFF){
		super(p, i, d, vFF, aFF, 0, 0);
	}
	
	
	public void configureProfile(double dt, double maxVel, double maxAccel){
		this.profileConfig.deltaTime = dt ;
		this.profileConfig.maxVelocity = maxVel;
		this.profileConfig.maxAcceleration = maxAccel;
		this.profileConfig.maxDeceleration = maxAccel;
	}

	@Override
	protected void calculateTrapezoid(){
		this.deltaTime = profileConfig.deltaTime;
		if(this.timerReset){
			this.lastTime = System.currentTimeMillis();
		}else{
			double currentTime = System.currentTimeMillis();
			this.deltaTime = currentTime - lastTime;
			this.lastTime = currentTime; 
		}
		this.deltaTime = this.deltaTime/1000.0;
		
		if(this.isDone()){ // we are at the target 
			this.currentSetpoint.position = this.desiredVal; // stop at desired val
			this.currentSetpoint.velocity = 0;
			this.currentSetpoint.acceleration =0;
		}else{ // calculate the new position, velocity, and acceleration
			double distanceFromTarget = this.desiredVal - this.currentSetpoint.position;
			double currentVelocity = this.currentSetpoint.velocity;
			double currentVelocitySquared = currentVelocity * currentVelocity;
			boolean goingBackwards = false;
			
			if(distanceFromTarget < 0){
				goingBackwards = true;
				distanceFromTarget *= -1;
				currentVelocity *= -1;
			}
			
			// Calculate the minimum and maximum reachable velocity with the remaining distance
			double maxReachableVelocityDisc = currentVelocitySquared / 2.0 + this.profileConfig.maxAcceleration * distanceFromTarget;
			double minReachableVelocityDisc = currentVelocitySquared / 2.0 - this.profileConfig.maxDeceleration * distanceFromTarget;
			double cruiseVelocity = currentVelocity;
			
			if(minReachableVelocityDisc < 0 || cruiseVelocity < 0){ // going backwards
				cruiseVelocity = Math.min(this.profileConfig.maxVelocity, Math.sqrt(maxReachableVelocityDisc));
			}
			
			
			double rampUpTime = (cruiseVelocity - currentVelocity) / this.profileConfig.maxAcceleration; // how long to accelerate 
			double rampUpDistance = currentVelocity * rampUpTime + (0.5 * this.profileConfig.maxAcceleration * rampUpTime * rampUpTime); // position 
			
			double cruiseDistance = Math.max(0, distanceFromTarget - rampUpDistance); // how long to cruise 
			double cruiseTime = Math.abs(cruiseDistance / cruiseVelocity); 
			
			// Calculate where we should be one cycle into the future
			if(rampUpTime >= this.deltaTime){ // we are in the acceleration phase
				System.out.println("AccelPhase");
				this.nextState.position = (currentVelocity * this.deltaTime + (0.5 * this.profileConfig.maxAcceleration * this.deltaTime * this.deltaTime));
				this.nextState.velocity = (currentVelocity + this.profileConfig.maxAcceleration * this.deltaTime);
				this.nextState.acceleration = this.profileConfig.maxAcceleration;
			}else if(rampUpTime + cruiseTime >= this.deltaTime){ // at cruise velocity phase
				System.out.println("CruisePhase");
				this.nextState.position = rampUpDistance + cruiseVelocity * (this.deltaTime - rampUpTime);
				this.nextState.velocity = cruiseVelocity;
				this.nextState.acceleration= 0;
			}else{ // we have finished the trajectory
				System.out.println("DonePhase");
				this.nextState.position = distanceFromTarget;
				this.nextState.velocity = 0;
				this.nextState.acceleration = 0;
			}
			
			if(goingBackwards){ // flip everything if we are going backwards
				this.nextState.position = -1*this.nextState.position;
				this.nextState.velocity = -1*this.nextState.velocity;
				this.nextState.acceleration = -1*this.nextState.acceleration;
			}
			
			
			// add the future to the current
			this.currentSetpoint.position += this.nextState.position;
			this.currentSetpoint.velocity = this.nextState.velocity;
			this.currentSetpoint.acceleration = this.nextState.acceleration;
		}
	}
	
	@Override
	public double calculate(double position, double velocity){
		this.calculateTrapezoid();
		return super.calculateOutput(position, velocity);
		
		
	}
	
	@Override
	public boolean isDone(){
		if(this.desiredVal - this.currentSetpoint.position > 0){ // going forwards
			return (this.currentSetpoint.position >= this.desiredVal); // return true when passed the setpoint
		}else{ // going backwards
			return (this.currentSetpoint.position <= this.desiredVal); // return true when behind the setpoint 
		}	
		 
	}

	
}
