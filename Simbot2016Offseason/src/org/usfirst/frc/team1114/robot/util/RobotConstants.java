package org.usfirst.frc.team1114.robot.util;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class RobotConstants {
	
	public static double DRIVE_MAX_VEL = 12; // fps
	public static double DRIVE_MAX_ACCEL = 5;
	public static double DRIVE_MAX_DECEL = 5;
	
	public static double GYRO_MAX_VEL = 10; // DPS
	public static double GYRO_MAX_ACCEL = 2.0;
	public static double GYRO_MAX_DECEL = 2.0;
	
	public static double gyroP = 0.0;
	public static double gyroI = 0.0;
	public static double gyroD = 0.0;
	public static double gyroKv = 1.0 / GYRO_MAX_VEL; // might need slight tweaking 
	public static double gyroKa = 0.0;
	public static double gyroEps = 0.5;
	
	public static double encP = 0.0;
	public static double encI = 0.0;
	public static double encD = 0.0;
	public static double encKv = 1.0 / DRIVE_MAX_VEL;
	public static double encKa = 0.0;
	public static double encEps = 0.5;


	
	public static void pushValues(){
		
		
		/*SmartDashboard.putNumber("Drive Gyro P: ", 0.035);
		SmartDashboard.putNumber("Drive Gyro I: ", 0.012);
		SmartDashboard.putNumber("Drive Gyro D: ", 0.35);
		SmartDashboard.putNumber("Drive Gyro Eps: ", 0.5);
		
		SmartDashboard.putNumber("Drive Enc P: ", 0.01);
		SmartDashboard.putNumber("Drive Enc I: ", 0.00);
		SmartDashboard.putNumber("Drive Enc D: ", 0.00);
		SmartDashboard.putNumber("Drive Enc Eps: ", 0.5);*/
		
		SmartDashboard.putBoolean("Image Use Binary", false);

		SmartDashboard.putNumber("Profile Gyro P: ", gyroP);
		SmartDashboard.putNumber("Profile Gyro I: ", gyroI);
		SmartDashboard.putNumber("Profile Gyro D: ", gyroD);
		SmartDashboard.putNumber("Profile Gyro VelFF: ", gyroKv);
		SmartDashboard.putNumber("Profile Gyro AcclFF: ", gyroKa);
		SmartDashboard.putNumber("Profile Gyro Eps: ", gyroEps);
		
		SmartDashboard.putNumber("Profile Enc P: ", encP);
		SmartDashboard.putNumber("Profile Enc I: ", encI);
		SmartDashboard.putNumber("Profile Enc D: ", encD);
		SmartDashboard.putNumber("Profile Enc VelFF: ", encKv);
		SmartDashboard.putNumber("Profile Enc AcclFF: ", encKa);
		SmartDashboard.putNumber("Profile Enc Eps: ", encEps);
		
		
		
		
	}
	
	
	
}
