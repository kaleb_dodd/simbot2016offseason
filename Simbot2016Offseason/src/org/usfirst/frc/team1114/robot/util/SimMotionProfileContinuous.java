package org.usfirst.frc.team1114.robot.util;

public class SimMotionProfileContinuous extends SimMotionProfile {
	//This class is used to keep regenerating a new profile once the old one is finished
	//used for teleop and non finishing auton commands
	
	public SimMotionProfileContinuous(double p, double i, double d, double vFF, double aFF, double epsRange,
			double epsVel) {
		super(p, i, d, vFF, aFF, epsRange, epsVel);
		
	}

	public SimMotionProfileContinuous(double p, double i, double d, double vFF, double aFF, double gravityFF,
			double epsRange, double epsVel) {
		super(p, i, d, vFF, aFF, gravityFF, epsRange, epsVel);
		
	}
	
	public boolean isDone(double position, double velocity, double acceleration){
		boolean isDone = false;
		if((Math.abs(this.currentSetpoint.position - this.desiredVal) <= this.finishedRange) && 
				(Math.abs(this.currentSetpoint.velocity) <= this.finishedVelocity)){
			isDone = true;
		}
		
		if(isDone){ // reset the profile
			super.setDesiredValue(position, velocity, acceleration, this.desiredVal);
		}
		return isDone;
	}

	
}
