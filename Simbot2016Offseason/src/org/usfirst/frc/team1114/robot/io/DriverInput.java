package org.usfirst.frc.team1114.robot.io;

import org.usfirst.frc.team1114.robot.util.LogitechF310Gamepad;

public class DriverInput {

	private static DriverInput instance;
	
	private LogitechF310Gamepad driver;
	
	private boolean autonIncreaseStepWasPressed = false;
    private boolean autonDecreaseStepWasPressed = false;
    
    private boolean autonIncreaseModeWasPressed = false;
    private boolean autonDecreaseModeWasPressed = false;
    
    private boolean autonIncreaseMode10WasPressed = false;
    private boolean autonDecreaseMode10WasPressed = false;
	
	
	private DriverInput(){
		this.driver = new LogitechF310Gamepad(0);
	}
	
	public static DriverInput getInstance(){
		if(instance == null){
			instance = new DriverInput();
		}
		return instance;
	}
	
	//DRIVER CONTROLS 
	public double getDriverX(){
		return this.driver.getLeftX();
	}
	
	public double getDriverY(){
		return this.driver.getLeftY();
	}
	
	public double getDriverRightX(){
		return this.driver.getRightX();
	}
	
	public double getDriverRightY(){
		return this.driver.getRightY();
	}
	
	public boolean getDriverArmPickupButton(){
    	return this.driver.getGreenButton();
    }
    
    public boolean getDriverArmMidButton(){
    	return this.driver.getRedButton();
    }
    
    public boolean getDriverArmHighButton(){
    	return this.driver.getYellowButton();
    }
    
    public boolean getDriverHighGearButton(){
    	return this.driver.getLeftBumper();
    }
    
    public boolean getDriverLowGearButton(){
    	return this.driver.getLeftTrigger() > 0.3;
    }
    
	
	//AUTO SELECTION CONTROLS
	
	public boolean getAutonSetDelayButton(){
		return this.driver.getRightTrigger() > 0.3;
	}
	
	public double getAutonDelayStick(){
		return this.driver.getLeftY();
	}
	
	
	
	public boolean getAutonStepIncrease() {
    	// only returns true on rising edge
    	boolean result = this.driver.getRightBumper() && !this.autonIncreaseStepWasPressed;
    	this.autonIncreaseStepWasPressed = this.driver.getRightBumper();
    	return result;
    	
    }
	
	public boolean getAutonStepDecrease() {
    	// only returns true on rising edge
    	boolean result = this.driver.getLeftBumper() && !this.autonDecreaseStepWasPressed;
    	this.autonDecreaseStepWasPressed = this.driver.getLeftBumper();
    	return result;
    
    }
	
	public boolean getAutonModeIncrease() {
    	// only returns true on rising edge
    	boolean result = this.driver.getRedButton() && !this.autonIncreaseModeWasPressed;
    	this.autonIncreaseModeWasPressed = this.driver.getRedButton();
    	return result;
    	
    }
    
    public boolean getAutonModeDecrease() {
    	// only returns true on rising edge
    	boolean result = this.driver.getGreenButton() && !this.autonDecreaseModeWasPressed;
    	this.autonDecreaseModeWasPressed = this.driver.getGreenButton();
    	return result;
    
    }
    
    public boolean getAutonModeIncreaseBy10() {
    	// only returns true on rising edge
    	boolean result = this.driver.getYellowButton() && !this.autonIncreaseMode10WasPressed;
    	this.autonIncreaseMode10WasPressed = this.driver.getYellowButton();
    	return result;
    	
    }
    
    public boolean getAutonModeDecreaseBy10() {
    	// only returns true on rising edge
    	boolean result = this.driver.getBlueButton() && !this.autonDecreaseMode10WasPressed;
    	this.autonDecreaseMode10WasPressed = this.driver.getBlueButton();
    	return result;
    
    }
	
	
	
	
	
	
}
