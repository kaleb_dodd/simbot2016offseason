package org.usfirst.frc.team1114.robot.io;

import org.usfirst.frc.team1114.robot.util.SimEncoder;
import org.usfirst.frc.team1114.robot.util.SimNavx;

import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class SensorInput {
	
	private static SensorInput instance;
	
	private SimEncoder encDriveLeft;
	private SimEncoder encDriveRight;
	private SimEncoder encArm;
	
	private SimNavx navx;
	
	private PowerDistributionPanel pdp;
	
	private double lastTime = 0.0;
	private double deltaTime = 20.0;
	
	private boolean firstCycle = true;
	
	private static double TICKSPERINCH = (128*3) * (36.0/48.0) / (Math.PI*4.125); // drive
	private double leftDriveSpeedFPS;
	private double rightDriveSpeedFPS;
	
	private double lastLeftDriveSpeedFPS = 0;
	private double lastRightDriveSpeedFPS = 0;
	private double leftDriveAccelerationFPSSquared; 
	private double rightDriveAccelerationFPSSquared; 
	
	private double gyroAngle;
	private double lastGyroAngle;
	private double gyroVelocity = 0; // degrees per second
	private double lastGyroVelocity =0;
	private double gyroAcceleration =0; 
	
	private double drivePositionState;
	private double driveVelocityState;
	private double driveAccelerationState;
	
	private double gyroPositionState;
	private double gyroVelocityState;
	private double gyroAccelerationState;
	
	
	
	private SensorInput(){
		this.encDriveLeft = new SimEncoder(1,0);
		this.encDriveRight = new SimEncoder(3,2);
		this.encArm = new SimEncoder(6,7);
		this.navx = new SimNavx();
		this.pdp = new PowerDistributionPanel();
				
		
		this.reset();
	}
	
	public static SensorInput getInstance() {
        if(instance == null) {
            instance = new SensorInput();
        }
        return instance;
    }
	
	public void reset(){
		this.navx.reset();
		this.encDriveLeft.reset();
		this.encDriveRight.reset();
		this.encArm.reset();
		this.firstCycle = true;
	}
	
	public void update(){
		if (this.lastTime == 0.0) {
    		this.deltaTime = 20;
    		this.lastTime = System.currentTimeMillis();
    	} else {
    		this.deltaTime = System.currentTimeMillis() - this.lastTime;
    		this.lastTime = System.currentTimeMillis();
    	}
		
		this.navx.update();
		
    	if(this.firstCycle){
    		this.firstCycle = false;
    		this.lastGyroAngle = this.navx.getAngle();
    	}
    	
    	this.encDriveLeft.updateSpeed();
    	this.encDriveRight.updateSpeed();
    	this.encArm.updateSpeed();
    	
    	double left = this.getEncoderLeftSpeed() / TICKSPERINCH;
    	double right = this.getEncoderRightSpeed() / TICKSPERINCH;
    	
    	this.leftDriveSpeedFPS = (left/12)/(this.deltaTime/1000);
		this.rightDriveSpeedFPS =  (right/12)/(this.deltaTime/1000);
    	
		this.leftDriveAccelerationFPSSquared = (this.leftDriveSpeedFPS-this.lastLeftDriveSpeedFPS)/(this.deltaTime/1000);
		this.rightDriveAccelerationFPSSquared = (this.rightDriveSpeedFPS-this.lastRightDriveSpeedFPS)/(this.deltaTime/1000);
    	
		this.lastLeftDriveSpeedFPS = this.leftDriveSpeedFPS;
    	this.lastRightDriveSpeedFPS = this.rightDriveSpeedFPS;
		
    	this.gyroAngle = this.navx.getAngle();
    	this.gyroVelocity = (this.gyroAngle-this.lastGyroAngle)/(this.deltaTime/1000); // gyro speed in degrees per second
    	this.lastGyroAngle = this.gyroAngle; 

    	this.gyroAcceleration = (this.gyroVelocity - this.lastGyroVelocity) / (this.deltaTime/1000);
    	this.lastGyroVelocity = this.gyroVelocity;
    	
    	this.drivePositionState = this.getDriveFeet();
    	this.driveVelocityState= this.getDriveSpeedFPS();
    	this.driveAccelerationState= this.getDriveAcceleration();
    	
    	this.gyroPositionState= this.getAngle();
    	this.gyroVelocityState= this.getGyroVelocity();
    	this.gyroAccelerationState= this.getGyroAcceleration();
    	
    	
    	SmartDashboard.putNumber("DRIVE POSITION FEET: ", this.getDriveFeet());
    	SmartDashboard.putNumber("DRIVE SPEED: ", this.getDriveSpeedFPS());
    	SmartDashboard.putNumber("DRIVE ACCEL: ", this.getDriveAcceleration());
    	
    	SmartDashboard.putNumber("GYRO SPEED: ", this.getGyroVelocity());
    	SmartDashboard.putNumber("GYRO ACCEL: ", this.getGyroAcceleration());
    	
    	
    	
    	SmartDashboard.putNumber("Left Enc",this.getEncoderLeft());
    	SmartDashboard.putNumber("Right Enc",this.getEncoderRight());
    	SmartDashboard.putNumber("Intake Arm Enc: ", this.getIntakeArmEnc());
    	SmartDashboard.putNumber("Gyro: ", this.getAngle());
    	
    	
	}
	
	public double getDeltaTime(){
		return this.deltaTime;
	}
	
	public double getDrivePositionState(){
		return this.drivePositionState;
	}
	
	public double getDriveVelocityState(){
		return this.driveVelocityState;
	}
	
	
	public double getDriveAccelerationState(){
		return this.driveAccelerationState;
	}
	
	public double getGyroPositionState(){
		return this.gyroPositionState;
	}
	
	public double getGyroVelocityState(){
		return this.gyroVelocityState;
	}
	
	
	public double getGyroAccelerationState(){
		return this.gyroAccelerationState;
	}
	
	
	public double getLastTickLength() {
    	return this.deltaTime;
    }
	
	// DRIVE //
	
	public int getEncoderLeft() {
    	return this.encDriveLeft.get();
    }
	
	public int getEncoderLeftSpeed() {
    	return this.encDriveLeft.speed();
    }
	
	public double getLeftDriveInches(){
		return this.getEncoderLeft() / TICKSPERINCH;
	}
	    
	public double getLeftDriveSpeedInches(){
		return this.getEncoderLeftSpeed() / TICKSPERINCH;
	}
	
	public int getEncoderRight() {
    	return this.encDriveRight.get();
    }
	
	public int getEncoderRightSpeed() {
    	return this.encDriveRight.speed();
    }

    public double getRightDriveInches(){
    	return this.getEncoderRight() / TICKSPERINCH;
    }

    public double getRightDriveSpeedInches(){
    	return this.getEncoderRightSpeed() / TICKSPERINCH;
    }
    
    public double getDriveEncoderAverage() {
    	return (this.getEncoderRight() + this.getEncoderLeft())/2;
    }
    
    public double getDriveInches(){
    	return this.getDriveEncoderAverage() / TICKSPERINCH;
    }
    
    public double getDriveEncoderSpeedAverage(){
    	return (this.getEncoderLeftSpeed() + this.getEncoderRightSpeed()) / 2.0;
    }
    
    public double getLeftDriveSpeedFPS(){
    	return this.leftDriveSpeedFPS;
    }
    
    public double getRightDriveSpeedFPS(){
    	return this.rightDriveSpeedFPS; 
    }
    
    public double getDriveFeet(){
    	return this.getDriveInches()/12; 
    }
    
    public double getDriveSpeedFPS(){
    	return (this.leftDriveSpeedFPS+this.rightDriveSpeedFPS)/2;
    }
    
    public double getLeftDriveAcceleration(){
    	return this.leftDriveAccelerationFPSSquared;
    }
    
    public double getRightDriveAcceleration(){
    	return this.rightDriveAccelerationFPSSquared;
    }
    
    public double getDriveAcceleration(){
    	return (this.getLeftDriveAcceleration() + this.getRightDriveAcceleration())/2; 
    }
    
    // INTAKE // 
    public void resetIntakeArmEnc(){
		this.encArm.reset();
	}
    
	public int getIntakeArmEnc(){
		return this.encArm.get();
	}
	
	public double getIntakeArmEncSpeed(){
		return this.encArm.speed();
	}
    
    
	
	// GYRO //
	
	public double getAngle(){
    	return this.navx.getAngle();
    }
	
	public double getGyroVelocity(){
		return this.gyroVelocity;
	}
    
	public double getGyroAcceleration(){
		return this.gyroAcceleration;
	}
    public double getAbsoluteAngle() {
    	return 360 - this.navx.getFusedHeading();
    }
    
    public double getZAxisValue() {
    	return this.navx.getRoll();
    }
    
    // PDP //
    
    public double getVoltage(){
    	return this.pdp.getVoltage();
    }
    
    public double getCurrent(int port){
    	return this.pdp.getCurrent(port);
    }
    
    
    
}
