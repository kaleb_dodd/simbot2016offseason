package org.usfirst.frc.team1114.robot.io;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.VictorSP;

public class RobotOutput {
	private static RobotOutput instance;
	
	private VictorSP driveLeftFront;
	private VictorSP driveLeftBack;
	private VictorSP driveRightFront;
	private VictorSP driveRightBack; 
	
	private VictorSP intakeArmLeft;
	private VictorSP intakeArmRight; 
	
	private Solenoid lowGear;
	private Solenoid highGear;
	private boolean inHighGear = false; 
	
	
	private RobotOutput(){
		this.driveLeftFront = new VictorSP(0);
		this.driveLeftBack = new VictorSP(1);
		this.driveRightFront = new VictorSP(2);
		this.driveRightBack = new VictorSP(3);
		
		this.intakeArmLeft = new VictorSP(5);
		this.intakeArmRight = new VictorSP(6);
		
		this.lowGear = new Solenoid(0);
		this.highGear = new Solenoid(1);
		
	}
	
	public static RobotOutput getInstance(){
		if(instance == null){
			instance = new RobotOutput();
		}
		return instance;
	}
	
	//Motor Commands
	
	//Drive
	public void setDriveLeft(double output){
		this.driveLeftFront.set(-output);
		this.driveLeftBack.set(-output);
	}
	
	public void setDriveRight(double output){
		this.driveRightFront.set(output);
		this.driveRightBack.set(output);
	}
	
	//Arm
	public void setIntakeArm(double val){
		this.intakeArmLeft.set(val);
		this.intakeArmRight.set(-val);
	}
	
	public void setHighGear(boolean isHigh){
		this.lowGear.set(!isHigh);
		this.highGear.set(isHigh);
		this.inHighGear = isHigh;
	}
	
	
	public void stopAll() {
    	setDriveLeft(0);
    	setDriveRight(0);
    	setIntakeArm(0);
    	// shut off things here
    }
	
	
}
